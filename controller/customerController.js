const customerController = {}
const { consumer, event, order} = require("../model/customerSchema")

customerController.makepayment = async (req,res)=>{
    try {
        let customer = await event.find({consumer_no:req.body.consumer_no})
        if(!customer.length){
            const eventData = new event(req.body)
            const dateObject = new Date();
    let date = dateObject.toUTCString();
            eventData.device_ts = date
            await eventData.save()
            // console.log(eventData)
            return res.status(200).send({message:"order created succufully"})
        }else{
const updateEvent = await event.updateMany({
consumer_no:req.body.consumer_no  
},{$set: {state:req.body.state}}
)
return res.status(200).send({message:"event updated succesfully"})
        }
    } catch (error) {
        return res.status(400).send(error.message)
    }
}


customerController.orderstatus = async(req,res)=>{
    try {
        const orderData = await order.find({}).lean()
        let mapOrderNo = orderData.map((i)=>{return i.order_no})
        const consumerData = await order.find({consumer_no:req.body.consumer_no})
        if(mapOrderNo.includes(req.body.order_no)){
            return res.status(200).send({message:"order numbers should be unique"})
        }
        else if(!consumerData.length){
            const orderData = new order(req.body)
            await orderData.save()
            return res.status(201).send({message:"order created succufully"})
        }
        else{
            if(req.body.orderstatus === "ISSUE"){
                const updateEvent = await event.updateMany({consumer_no:req.body.consumer_no},
                    {$set:{
                        state:"Inactive"
                    }})
return res.status(200).send({message:"event status updated succesfully"})
            }else{
                const updateOrder = await order.updateMany({consumer_no:req.body.consumer_no},
                    {$set:{
                        order_status:"DONE"
                    }})
return res.status(200).send({message:"order status updated succesfully"})
            }
        }
    } catch (error) {
        return res.status(400).send(error.message)
    }
}

module.exports = customerController