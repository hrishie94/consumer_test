const express = require("express")
const app = express();
const cron = require("node-cron")
const port = 3000;
app.use(express.json())
const { order ,event} = require("./model/customerSchema")
const connectdb = require("./db/connection")
connectdb();
const customerrouter =  require("./route/route")
app.use("/",customerrouter)



async function orders(){
    let orderdata = await event.find({remarks:"Unpaid"})
    if(orderdata){
        let eventData = await order.updateMany(
            {consumer_no:orderdata.consumer_no},
                    {$set:{
                        order_status:"ISSUE"
                    }})
        
    }
}


cron.schedule("0 0 1-10 * *", orders);


app.listen(port,()=>{
    console.log("the server is running at port 3000")
})
