const express = require("express")
const router = express.Router();

let customerController = require("../controller/customerController")

router.post("/makepayment",customerController.makepayment)

router.post("/orderstatus",customerController.orderstatus)


module.exports = router