const mongoose = require("mongoose")
// require("dotenv").config();
const URI = "mongodb+srv://hrishi:ethape@cluster0.swmc0ij.mongodb.net/"
mongoose.set('strictQuery', false);
const connectDB = async () => {
    await mongoose.connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    console.log('the mongodb database has connected')
}
module.exports = connectDB;
