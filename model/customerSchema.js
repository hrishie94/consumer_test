const mongoose = require("mongoose")
const Schema = mongoose.Schema

const cardSchema = new Schema({
    consumer_no: { type: Number, default: require },
  name: { type: String, default: require },
  phone : { type: Number, default: require },
  createdAt : { type: String, default: require },
    updatedAt : { type: String, default: require },
    CONSTRAINT : { type: String, default: require }
});


const eventSchema  = new Schema({
  perfomed_by: { type: String, default: "Agent" },
    consumer_no:{ type: Number,default: require } ,
    consumername :{ type: String, default: require }, 
    state :{ type: String, default: require },
    remarks: { type: String, default: require },
    reason :{ type: String, default: require },
    reason_code :  { type: Number, default: require },
    order_no:{ type: Number, default: require },
    amount :{ type: Number, default: require },
    bill_year_month :{ type: String, default: require }, 
    device_ts :{ type: Number}
},   { timestamps: true })



const orderSchema  = new Schema({
    order_name:{ type: String, default: require },// -- DISCONNECTION
    consumer_no:{ type: Number, default: require },  
    order_no:{ type: Number, default: require },   //NOT NULL DEFAULT, -- should be a unique number when new order is generated
    amount:{ type: Number, default: require },  
    order_status:{ type: String, default: require }, // -- ISSUED, DONE, NOT_DONE, CLOSED.
    revision :{ type: Number, default: require }, //0,
    reason_code:{ type: Number, default: require }, // "default",
    bill_year_month:{ type: String, default: require }
  },   { timestamps: true })


let consumer = mongoose.model("consumer", cardSchema)
let event = mongoose.model("event", eventSchema)
let order = mongoose.model("order", orderSchema)


module.exports = { consumer, event, order}


// -- basic Consumer details will store in this table.
// CREATE TABLE IF NOT EXISTS public."Consumers"
// (
//     consumer_no character varying(255),
//     name character varying(255),
//     phone character varying(255),
//     "createdAt" timestamp with time zone NOT NULL,
//     "updatedAt" timestamp with time zone NOT NULL,
//     CONSTRAINT "Consumers_pkey" PRIMARY KEY (consumer_no)
// )
// -- This table contains Disconnection Notice / Order to the consumer.
// CREATE TABLE IF NOT EXISTS public."Orders"
// (
//     id SERIAL PRIMARY KEY,
//     order_name character varying(255) COLLATE pg_catalog."default", -- DISCONNECTION
//     consumer_no character varying(255) COLLATE pg_catalog."default", 
//     order_no character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT, -- should be a unique number when new order is generated
//     amount double precision,
//     order_status character varying(255) COLLATE pg_catalog."default", -- ISSUED, DONE, NOT_DONE, CLOSED.
//     revision integer DEFAULT 0,
//     reason_code character varying(255) COLLATE pg_catalog."default",
//     bill_year_month character varying(255) COLLATE pg_catalog."default",
//     "createdAt" timestamp with time zone NOT NULL,
//     "updatedAt" timestamp with time zone NOT NULL,
//     CONSTRAINT "Orders_pkey" PRIMARY KEY (id)
// )
// -- order_status: 
// -- ISSUED means when due to non-payment, system raised notice to any consumers.
// -- DONE means when agent visit and successfully, perform event at consumer end.
// -- NOT_DONE means when agent visits, but not able to perform event at consumer side.


// CREATE TABLE IF NOT EXISTS public."Events"
// (
//     id SERIAL PRIMARY KEY,
//     perfomed_by bigint,
//     consumer_no character varying(255) COLLATE pg_catalog."default",
//     name character varying(255) COLLATE pg_catalog."default", -- name like PAYMENT, DISCONNECTION_DONE, DISCONNECTION_NOT_DONE
//     state character varying(255) COLLATE pg_catalog."default",
//     remarks text COLLATE pg_catalog."default",
//     reason character varying(255) COLLATE pg_catalog."default",
//     reason_code character varying(255) COLLATE pg_catalog."default",
//     order_no character varying(255) COLLATE pg_catalog."default",
//     amount double precision,
//     bill_year_month character varying(255) COLLATE pg_catalog."default", -- possible value APR-23, MAR-23
//     device_ts timestamp with time zone,
//     "createdAt" timestamp with time zone NOT NULL,
//     "updatedAt" timestamp with time zone NOT NULL,
//     CONSTRAINT "Events_pkey" PRIMARY KEY (id)
// // 